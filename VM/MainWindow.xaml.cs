﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VM.MVVM;
using VM.ViewModels;

namespace VM
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

#if WEAKREF

            this.DataContext = new VM.VMWeakRef.MainViewModel();

#elif LONGLIVED

            this.DataContext = new VM.VMLongLived.MainViewModel();

#elif SINGLE

            this.DataContext = new VM.VMSingle.MainViewModel();

#endif
            this.Closing += (s, e) => ((ViewModelBase)this.DataContext).Dispose();

        }

    }

}

