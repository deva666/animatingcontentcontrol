﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using VM.MVVM;

namespace VM.VMWeakRef
{
    internal abstract class HostViewModel : ViewModelBase
    {
        private readonly Dictionary<Type, ViewModelResolver> _childrenMap = new Dictionary<Type, ViewModelResolver>();

        protected ViewModelBase _selectedChild;

        public ViewModelBase SelectedChild
        {
            get { return _selectedChild; }
            set
            {
                if (_selectedChild != null && _selectedChild.Title == value.Title)
                    return;

                SetPropertyValue(ref _selectedChild, value);
            }
        }

        protected void RegisterChild<T>(Func<T> getter) where T : ViewModelBase
        {
            Contract.Requires(getter != null);

            if (_childrenMap.ContainsKey(typeof(T)))
                return;

            var resolver = new ViewModelResolver(getter);
            _childrenMap.Add(typeof(T), resolver);
        }

        protected ViewModelBase GetChild(Type type)
        {
            Contract.Requires(type != null);
            if (_childrenMap.ContainsKey(type) == false)
                throw new InvalidOperationException("Can't resolve type " + type.ToString());

            var resolver = _childrenMap[type];
            ViewModelBase viewModel = null;
            resolver.Reference.TryGetTarget(out viewModel);
            if (viewModel == null)
            {
                viewModel = resolver.Getter();
                resolver.Reference.SetTarget(viewModel);
            }
            return viewModel;
        }

        private class ViewModelResolver
        {
            public WeakReference<ViewModelBase> Reference { get; private set; }
            public Func<ViewModelBase> Getter { get; private set; }

            public ViewModelResolver(Func<ViewModelBase> getter)
            {
                Reference = new WeakReference<ViewModelBase>(null);
                Getter = getter;
            }
        }
    }
}
