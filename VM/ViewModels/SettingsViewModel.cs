﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VM.MVVM;

namespace VM.ViewModels
{
    internal sealed class SettingsViewModel : ViewModelBase
    {
        private ICommand _startScrolling;
        public ICommand StartScrolling
        {
            get { return _startScrolling ?? (_startScrolling = new DelegateCommand(Scroll)); }
        }

        private void Scroll()
        {
            string[] infos = new string[] { "Info1", "Info2", "Info3", "Info4", "Info5" };
            foreach (var info in infos)
            {
                InfoContent = info;
            }

        }

        private string _infoContent;
        public string InfoContent
        {
            get { return _infoContent; }
            set { SetPropertyValue(ref _infoContent, value); }
        }

        public override string Title
        {
            get { return "Settings"; }
        }

        public SettingsViewModel()
        {
            SettingsEnabled = true;
        }

        private bool _settingsEnabled;
        public bool SettingsEnabled
        {
            get { return _settingsEnabled; }
            set { SetPropertyValue(ref _settingsEnabled, value); }
        }

        private bool _prefrence1;
        public bool Prefrence1
        {
            get { return _prefrence1; }
            set { SetPropertyValue(ref _prefrence1, value); }

        }

        private string _prefrence2;
        public string Prefrence2
        {
            get { return _prefrence2; }
            set { SetPropertyValue(ref _prefrence2, value); }
        }

        private double _prefrence3;
        public double Prefrence3
        {
            get { return _prefrence3; }
            set { SetPropertyValue(ref _prefrence3, value); }
        }
    }
}
