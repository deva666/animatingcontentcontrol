﻿using System.Collections.Generic;
using VM.DAL;
using VM.Models;
using VM.MVVM;


namespace VM.ViewModels
{
    internal sealed class AuthorsViewModel : ViewModelBase
    {
        private readonly IRepo<Author> _repo;

        public override string Title
        {
            get { return "Authors"; }
        }

        public AuthorsViewModel()
        {
            _repo = AuthorsRepo.Instance;
        }

        public List<Author> ContentSource
        {
            get { return _repo == null ? null : _repo.GetContent(); }
        }
    }
}
