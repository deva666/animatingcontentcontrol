﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VM.DAL;
using VM.Models;
using VM.MVVM;

namespace VM.ViewModels
{
    internal sealed class BooksViewModel : ViewModelBase
    {
        private readonly IRepo<Book> _repo;

        public override string Title
        {
            get { return "Books"; }
        }

        public BooksViewModel()
        {
            _repo = BooksRepo.Instance;
        }

        public List<Book> ContentSource
        {
            get { return _repo == null ? null : _repo.GetContent(); }
        }
    }
}
