﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using VM.MVVM;
using VM.ViewModels;

namespace VM.VMLongLived
{
    internal sealed class MainViewModel : HostViewModel
    {
        private readonly Timer _timer;

        public override string Title { get { return "Main"; } }

        public string Version
        {
            get { return this.GetType().Assembly.GetName().Version.ToString(); }
        }

        private string _time;
        public string Time
        {
            get { return _time; }
            set { SetPropertyValue(ref _time, value); }
        }

        private ICommand _goToBooksCommand;
        public ICommand GoToBooksCommand
        {
            get { return _goToBooksCommand ?? (_goToBooksCommand = new DelegateCommand(GoToBooks)); }
        }

        private ICommand _goToAuthorsCommand;
        public ICommand GoToAuthorsCommand
        {
            get { return _goToAuthorsCommand ?? (_goToAuthorsCommand = new DelegateCommand(GoToAuthors)); }
        }

        private ICommand _goToSettingsCommand;
        public ICommand GoToSettingsCommand
        {
            get { return _goToSettingsCommand ?? (_goToSettingsCommand = new DelegateCommand(GoToSettings)); }
        }

        public MainViewModel()
        {
            this.Children = new List<ViewModelBase>();
            GoToBooks();
            _timer = new Timer((s) => Time = DateTime.Now.ToLongTimeString(), this, 500, 500);
        }

        private void GoToBooks()
        {
            var booksVM = this.Children.FirstOrDefault(vm => vm.GetType() == typeof(BooksViewModel));
            if (booksVM == null)
            {
                booksVM = new BooksViewModel();
                Children.Add(booksVM);
            }
            this.SelectedChild = booksVM;
        }

        private void GoToAuthors()
        {
            var authorsVM = this.Children.FirstOrDefault(vm => vm.GetType() == typeof(AuthorsViewModel));
            if (authorsVM == null)
            {
                authorsVM = new AuthorsViewModel();
                Children.Add(authorsVM);
            }
            this.SelectedChild = authorsVM;
        }

        private void GoToSettings()
        {
            var settingsVM = this.Children.FirstOrDefault(vm => vm.GetType() == typeof(SettingsViewModel));
            if (settingsVM == null)
            {
                settingsVM = new SettingsViewModel();
                Children.Add(settingsVM);
            }
            this.SelectedChild = settingsVM;
        }

        protected override void OnDispose()
        {
            foreach (var vm in this.Children)
            {
                ((ViewModelBase)vm).Dispose();
            }
            _timer.Dispose();
            base.OnDispose();
        }
    }
}
