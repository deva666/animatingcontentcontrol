﻿using System.Collections.Generic;
using System.Windows.Input;
using VM.MVVM;

namespace VM.VMLongLived
{
    internal abstract class HostViewModel : ViewModelBase
    {
        private ViewModelBase _selectedChild;

        public ViewModelBase SelectedChild
        {
            get { return _selectedChild; }
            set { SetPropertyValue(ref _selectedChild, value); }
        }

        public List<ViewModelBase> Children { get; protected set; }
    }
}
