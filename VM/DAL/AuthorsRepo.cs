﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using VM.Models;
using VM.Utils;

namespace VM.DAL
{
    internal sealed class AuthorsRepo : IRepo<Author>
    {
        private List<Author> _authors;
        private static Lazy<AuthorsRepo> _instance = new Lazy<AuthorsRepo>(() => new AuthorsRepo());

        public static AuthorsRepo Instance
        {
            get { return _instance.Value; }
        }

        private AuthorsRepo()
        {
            _authors = GetAuthors(Constants.AUTHORS_XML_FILE);
        }

        private List<Author> GetAuthors(string xmlFile)
        {
            using (XmlReader reader = new XmlTextReader(xmlFile))
            {
                return (from book in XDocument.Load(reader).Element("authors").Elements("author")
                        select new Author()
                        {
                            Name = (string)book.Attribute("name"),
                            BirthYear = (string)book.Attribute("birthYear")
                        }).ToList();
            }
        }

        public List<Author> GetContent()
        {
            return _authors;
        }
    }
}
