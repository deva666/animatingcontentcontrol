﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VM.Models;
using VM.MVVM;
using VM.ViewModels;

namespace VM.VMSingle
{
    internal sealed class MainViewModel : HostViewModel
    {
        private readonly Timer _timer;

        public override string Title { get { return "Main"; } }

        public string Version
        {
            get { return this.GetType().Assembly.GetName().Version.ToString(); }
        }

        private string _time;
        public string Time
        {
            get { return _time; }
            set { SetPropertyValue(ref _time, value); }
        }

        private ICommand _goToBooksCommand;
        public ICommand GoToBooksCommand
        {
            get { return _goToBooksCommand ?? (_goToBooksCommand = new DelegateCommand(GoToBooks)); }
        }

        private ICommand _goToAuthorsCommand;
        public ICommand GoToAuthorsCommand
        {
            get { return _goToAuthorsCommand ?? (_goToAuthorsCommand = new DelegateCommand(GoToAuthors)); }
        }

        private ICommand _goToSettingsCommand;
        public ICommand GoToSettingsCommand
        {
            get { return _goToSettingsCommand ?? (_goToSettingsCommand = new DelegateCommand(GoToSettings)); }
        }

        public MainViewModel()
        {
            this.RegisterChild<BooksViewModel>(() => new BooksViewModel());
            this.RegisterChild<AuthorsViewModel>(() => new AuthorsViewModel());
            this.RegisterChild<SettingsViewModel>(() => new SettingsViewModel());

            this.SelectedChild = GetChild(typeof(BooksViewModel));

            _timer = new Timer((s) => Time = DateTime.Now.ToLongTimeString(), this, 500, 500);
        }

        private void GoToBooks()
        {
            this.SelectedChild = GetChild(typeof(BooksViewModel));
        }

        private void GoToAuthors()
        {
            this.SelectedChild = GetChild(typeof(AuthorsViewModel));
        }

        private void GoToSettings()
        {
            this.SelectedChild = GetChild(typeof(SettingsViewModel));
        }

        protected override void OnDispose()
        {
            if (this.SelectedChild != null)
                ((ViewModelBase)SelectedChild).Dispose();
            _timer.Dispose();
            base.OnDispose();
        }
    }
}
